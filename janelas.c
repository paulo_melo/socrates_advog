/*
Construindo janelas manualmente (usando o builder).
Solucionando problemas, assim a gente sabe o que est� acontecendo.
Cada janela em uma fun��o, fica mais organizado.
[by Paulo e Tain�]
[Equipe GREENSLEEVES]
*/
#include <gtk/gtk.h>
#include "structsGtk.h"

void buildMain ( GtkBuilder *builder, wMain *widgetsMain )
{
/*Construindo a janela principal*/
widgetsMain->window     = GTK_WIDGET ( gtk_builder_get_object(builder, (gchar*) "mainwindow"));
widgetsMain->statusBar  = GTK_WIDGET ( gtk_builder_get_object(builder, "statusbarMain"));

    /*Bot�es da toolbar principal*/
    widgetsMain->bCadastro  = GTK_TOOL_BUTTON ( gtk_builder_get_object(builder, "butCadastro") );
    widgetsMain->bLogin     = GTK_TOOL_BUTTON ( gtk_builder_get_object(builder, "butLogin")    );
    widgetsMain->bLogout    = GTK_TOOL_BUTTON ( gtk_builder_get_object(builder, "butLogout")   );
    widgetsMain->bSair      = GTK_TOOL_BUTTON ( gtk_builder_get_object(builder, "butSair")     );

    /*Campos de busca*/
    widgetsMain->eBuscaAdvog   = GTK_ENTRY ( gtk_builder_get_object(builder, "entBuscaAdvog")  );
    widgetsMain->eBuscaCliente = GTK_ENTRY ( gtk_builder_get_object(builder, "entBuscaCliente"));
    widgetsMain->eBuscaProcess = GTK_ENTRY ( gtk_builder_get_object(builder, "entBuscaProcss") );

    /*Bot�es das abas*/
        /*Cliente*/
        widgetsMain->bCliAdd   = GTK_TOOL_BUTTON ( gtk_builder_get_object(builder, "butClienteAdd")    );
        widgetsMain->bCliDel   = GTK_BUTTON ( gtk_builder_get_object(builder, "butClienteApagar") );
        widgetsMain->bBuscaCli = GTK_BUTTON ( gtk_builder_get_object(builder, "butBuscaCliente")  );

        /*Processos*/
        widgetsMain->bProAdd   = GTK_TOOL_BUTTON ( gtk_builder_get_object(builder, "butProcsAdd")    );
        widgetsMain->bProDel   = GTK_BUTTON ( gtk_builder_get_object(builder, "butProcsApagar") );
        widgetsMain->bProBusca = GTK_BUTTON ( gtk_builder_get_object(builder, "butBuscaProcss") );
        widgetsMain->bProPDF   = GTK_BUTTON ( gtk_builder_get_object(builder, "btnPDF")         );

        /*Advogados*/
        widgetsMain->bAdvAdd   = GTK_TOOL_BUTTON ( gtk_builder_get_object(builder, "butAdvogAdd")    );
        widgetsMain->bAdvDel   = GTK_BUTTON ( gtk_builder_get_object(builder, "butAdvogApagar") );
        widgetsMain->bBuscaAdv = GTK_BUTTON ( gtk_builder_get_object(builder, "butBuscaAdvog")  );
}

void buildLogin ( GtkBuilder *builder, wLogin *widgetsLogin )
{
/*Construindo janela do login*/
widgetsLogin->window     =  GTK_WIDGET ( gtk_builder_get_object (builder, "diaLogin") );

    /*Mensagem de erro*/
    widgetsLogin->labelError =  GTK_LABEL  ( gtk_builder_get_object (builder, "labErroLogin") );
    widgetsLogin->imageError =  GTK_WIDGET ( gtk_builder_get_object (builder, "imErroLogin")   );

    /*Bot�es*/
    widgetsLogin->button1    =  GTK_BUTTON ( gtk_builder_get_object (builder, "butOkLogin")       );
    widgetsLogin->button2    =  GTK_BUTTON ( gtk_builder_get_object (builder, "butCancelarLogin") );

    /*Entradas de texto*/
    widgetsLogin->entry1     =  GTK_ENTRY ( gtk_builder_get_object (builder, "entLogin") );
    widgetsLogin->entry2     =  GTK_ENTRY ( gtk_builder_get_object (builder, "entSenha") );
}

void buildAdvog (GtkBuilder *builder, wAddAdvog *widgetsAdvog)
{
/*Construindo janela de adicionar/editar advogado*/
widgetsAdvog->window    =  GTK_WIDGET ( gtk_builder_get_object (builder, "winAddAdvogado") );

    /*Mensagem de erro*/
    widgetsAdvog->labelError =  GTK_LABEL  (gtk_builder_get_object (builder, "labErroAdvog"));
    widgetsAdvog->imageError =  GTK_WIDGET (gtk_builder_get_object (builder, "imErroAdvog") );

    /*Bot�es*/
    widgetsAdvog->bOk        =  GTK_BUTTON (gtk_builder_get_object (builder, "butSalvarAdvog")  );
    widgetsAdvog->bCan       =  GTK_BUTTON (gtk_builder_get_object (builder, "butCancelarAdvog"));

    /*Entradas de texto*/
    widgetsAdvog->eOAB       =  GTK_ENTRY (gtk_builder_get_object (builder, "entCodigoAdvog")  );
    widgetsAdvog->eName      =  GTK_ENTRY (gtk_builder_get_object (builder, "entNomeAdvog")    );
    widgetsAdvog->eCPF       =  GTK_ENTRY (gtk_builder_get_object (builder, "entCpfAdvog")     );
    widgetsAdvog->eEnd       =  GTK_ENTRY (gtk_builder_get_object (builder, "entEnderecoAdvog"));
    widgetsAdvog->eTel       =  GTK_ENTRY (gtk_builder_get_object (builder, "entTelefoneAdvog"));
    widgetsAdvog->eEmail     =  GTK_ENTRY (gtk_builder_get_object (builder, "entMailAdvog")    );
}

void buildCliente (GtkBuilder *builder, wAddCliente *widgetsCliente)
{
/*Construindo janela de adicionar/editar cliente*/
widgetsCliente->window    =  GTK_WIDGET (gtk_builder_get_object (builder, "winAddCliente"));

    /*Mensagem de erro*/
    widgetsCliente->labelError =  GTK_LABEL  (gtk_builder_get_object (builder, "labErroCliente"));
    widgetsCliente->imageError =  GTK_WIDGET (gtk_builder_get_object (builder, "imErroCliente") );

    /*Bot�es*/
    widgetsCliente->bOk        =  GTK_BUTTON (gtk_builder_get_object (builder, "butSalvarCliente")  );
    widgetsCliente->bCan       =  GTK_BUTTON (gtk_builder_get_object (builder, "butCancelarCliente"));

    /*Entradas de texto*/
    widgetsCliente->eName      =  GTK_ENTRY (gtk_builder_get_object (builder, "entNomeCliente")    );
    widgetsCliente->eCpf       =  GTK_ENTRY (gtk_builder_get_object (builder, "entCpfCliente")     );
    widgetsCliente->eEnd       =  GTK_ENTRY (gtk_builder_get_object (builder, "entEnderecoCliente"));
    widgetsCliente->eMail      =  GTK_ENTRY (gtk_builder_get_object (builder, "entMailCliente")    );
    widgetsCliente->eTel       =  GTK_ENTRY (gtk_builder_get_object (builder, "entTelefoneCliente"));
}

void buildEditar (GtkBuilder *builder, wEditCadastro *widgetsEdit)
{
/*Construindo janela de editar cadastro*/
widgetsEdit->window    =  GTK_WIDGET (gtk_builder_get_object (builder, "winEditarCadastro"));

    /*Mensagem de erro*/
    widgetsEdit->labelError =  GTK_LABEL  (gtk_builder_get_object (builder, "labErroEdit"));
    widgetsEdit->imageError =  GTK_WIDGET (gtk_builder_get_object (builder, "imErroEdit") );

    /*Bot�es*/
    widgetsEdit->bOk        =  GTK_BUTTON (gtk_builder_get_object (builder, "butSalvarEdit")  );
    widgetsEdit->bCan       =  GTK_BUTTON (gtk_builder_get_object (builder, "butCancelarEdit"));

    /*Entradas de texto*/
    widgetsEdit->eOAB       =  GTK_ENTRY (gtk_builder_get_object (builder, "entEditCodigo")   );
    widgetsEdit->eName      =  GTK_ENTRY (gtk_builder_get_object (builder, "entEditNome")     );
    widgetsEdit->eCPF       =  GTK_ENTRY (gtk_builder_get_object (builder, "entEditCPF")      );
    widgetsEdit->eEnd       =  GTK_ENTRY (gtk_builder_get_object (builder, "entEditEndereco") );
    widgetsEdit->eSenha     =  GTK_ENTRY (gtk_builder_get_object (builder, "entEditSenha")    );
    widgetsEdit->eConf      =  GTK_ENTRY (gtk_builder_get_object (builder, "entEditConfirmar"));
    widgetsEdit->eTel       =  GTK_ENTRY (gtk_builder_get_object (builder, "entEditTelefone") );
    widgetsEdit->eEmail     =  GTK_ENTRY (gtk_builder_get_object (builder, "entEditEmail")    );
}

void buildModelo (GtkBuilder *builder, wNovoModelo *widgetsModelo)
{
/*Construindo janela de novo modelo*/
widgetsModelo->window    =  GTK_WIDGET (gtk_builder_get_object (builder, "winNovoModelo"));

    /*Bot�es*/
    widgetsModelo->bCan       =  GTK_BUTTON (gtk_builder_get_object (builder, "butModeloCancelar"));
    widgetsModelo->bOk        =  GTK_BUTTON (gtk_builder_get_object (builder, "butModeloSalvar")  );

    /*Entradas de texto*/
    widgetsModelo->eTitulo    =  GTK_ENTRY  (gtk_builder_get_object (builder, "entModeloTitulo"));
    widgetsModelo->eRG        =  GTK_ENTRY  (gtk_builder_get_object (builder, "entRgCliente")    );
    widgetsModelo->tCorpo     =  GTK_WIDGET (gtk_builder_get_object (builder, "txtModeloCorpo")  );
    widgetsModelo->eProto     =  GTK_ENTRY  (gtk_builder_get_object (builder, "entProtocolo")    );

    /*Labels de Erro*/
    widgetsModelo->labelError  = GTK_LABEL  (gtk_builder_get_object (builder, "labErroMod")    );
    widgetsModelo->imageError  = GTK_WIDGET (gtk_builder_get_object (builder, "imErroMod")    );
}

void buildEditAdvog ( GtkBuilder *builder, wEditAdvog *widgetsEAdvog )
{
/*Construindo janela de adicionar/editar advogado*/
widgetsEAdvog->window    =  GTK_WIDGET (gtk_builder_get_object (builder, "winEditAdvog"));

    /*Mensagem de erro*/
    widgetsEAdvog->labelError =  GTK_LABEL  (gtk_builder_get_object (builder, "labErroAdvogE"));
    widgetsEAdvog->imageError =  GTK_WIDGET (gtk_builder_get_object (builder, "imErroAdvogE") );

    /*Bot�es*/
    widgetsEAdvog->bOk        =  GTK_BUTTON (gtk_builder_get_object (builder, "butSalvarAdvogE")  );
    widgetsEAdvog->bCan       =  GTK_BUTTON (gtk_builder_get_object (builder, "butCancelarAdvogE"));
    widgetsEAdvog->bDel       =  GTK_BUTTON (gtk_builder_get_object (builder, "butDeletarAdvogE") );

    /*Entradas de texto*/
    widgetsEAdvog->eOAB       =  GTK_ENTRY (gtk_builder_get_object (builder, "entCodigoAdvogE"));
    widgetsEAdvog->eName      =  GTK_ENTRY (gtk_builder_get_object (builder, "entNomeAdvogE")  );
    widgetsEAdvog->eSenha     =  GTK_ENTRY (gtk_builder_get_object (builder, "entSenhaAdvogE") );
    widgetsEAdvog->eEmail     =  GTK_ENTRY (gtk_builder_get_object (builder, "entMailAdvogE")  );

}

void buildEditCliente (GtkBuilder *builder, wEditCliente *widgetsECliente)
{
/*Construindo janela de adicionar/editar cliente*/
widgetsECliente->window    =  GTK_WIDGET (gtk_builder_get_object (builder, "winEditCliente"));

    /*Mensagem de erro*/
    widgetsECliente->labelError =  GTK_LABEL  (gtk_builder_get_object (builder, "labErroClienteE"));
    widgetsECliente->imageError =  GTK_WIDGET (gtk_builder_get_object (builder, "imErroClienteE") );

    /*Bot�es*/
    widgetsECliente->bOk        =  GTK_BUTTON (gtk_builder_get_object (builder, "butSalvarClienteE")   );
    widgetsECliente->bCan       =  GTK_BUTTON (gtk_builder_get_object (builder, "butCancelarClienteE") );
    widgetsECliente->bDel       =  GTK_BUTTON (gtk_builder_get_object (builder, "butDeletarCliente")   );

    /*Entradas de texto carregadas*/
    widgetsECliente->eName      =  GTK_ENTRY (gtk_builder_get_object (builder, "entNomeClienteE")     );
    widgetsECliente->eRG        =  GTK_ENTRY (gtk_builder_get_object (builder, "entRgClienteE")       );
    widgetsECliente->eEnd       =  GTK_ENTRY (gtk_builder_get_object (builder, "entEnderecoClienteE") );
    widgetsECliente->eMail      =  GTK_ENTRY (gtk_builder_get_object (builder, "entMailClienteE")     );
    widgetsECliente->eClientId  =  GTK_ENTRY (gtk_builder_get_object (builder, "hiddenClienteIdE")     );
}

void buildEditModelo (GtkBuilder *builder, wEditModelo *widgetsEModelo)
{
/*Construindo janela de novo modelo*/
widgetsEModelo->window    =  GTK_WIDGET (gtk_builder_get_object (builder, "winEditModelo"));

    /*Bot�es*/
    widgetsEModelo->bCan     =  GTK_BUTTON (gtk_builder_get_object (builder, "butModeloCancelarE"));
    widgetsEModelo->bOk      =  GTK_BUTTON (gtk_builder_get_object (builder, "butModeloSalvarE")  );
    widgetsEModelo->bDel     =  GTK_BUTTON (gtk_builder_get_object (builder, "butModDel")  );
    widgetsEModelo->bPdf     =  GTK_BUTTON (gtk_builder_get_object (builder, "butPdfMod")  );

    /*Entradas de texto*/
    widgetsEModelo->eTitulo    =  GTK_ENTRY  (gtk_builder_get_object (builder, "entModeloTituloE"));
    widgetsEModelo->eRG        =  GTK_ENTRY  (gtk_builder_get_object (builder, "entRgClienteE")    );
    widgetsEModelo->tCorpo     =  GTK_WIDGET (gtk_builder_get_object (builder, "txtModeloCorpoE")  );
    widgetsEModelo->eProto     =  GTK_ENTRY  (gtk_builder_get_object (builder, "entProtocoloE")    );

    /*Labels de Erro*/
    widgetsEModelo->labErroMod  = GTK_LABEL (gtk_builder_get_object (builder, "labErroModE")    );
    widgetsEModelo->imErroMod   = GTK_WIDGET (gtk_builder_get_object (builder, "imErroModE")    );
}


void buildConf (GtkBuilder *builder, wConfirmar *widgetsConf)
{
/*Construindo janela de confirma��o*/
widgetsConf->window    =  GTK_WIDGET (gtk_builder_get_object (builder, "diaConfirmar"));

    /*Label*/
    widgetsConf->lConfirm   =  GTK_LABEL (gtk_builder_get_object (builder, "labConfirmar"));

    /*Bot�es*/
    widgetsConf->bSim       =  GTK_BUTTON (gtk_builder_get_object (builder, "butSimConfirmar"));
    widgetsConf->bNao       =  GTK_BUTTON (gtk_builder_get_object (builder, "butNaoConfirmar"));
}
