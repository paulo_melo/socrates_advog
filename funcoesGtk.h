#include "structsGtk.h"
/* ----- Mensagens de erro ----- */

#define AdvogJaExiste     "Advogado ja existe."
#define AdvogNaoExiste    "Advogado nao existe."
#define FalhaNoLogin      "Falha no login."
#define SenhaIncorreta    "Senha incorreta."

#define ClienteNaoExiste  "Cliente nao existe."
#define CpfClienteExiste  "RG existente."

#define ProNaoExiste     "Processo inexistente."
#define RegistroProcExist "Processo ja existe."

#define FalhaNoRegistro   "Falha no registro."

/*Todos os widgets que v�o ser acessados/modificados durante a execu��o tem que ser definidos como globais*/

wMain         widgetsMain;
wLogin        widgetsLogin;
wAddAdvog     widgetsAdvog;
wAddCliente   widgetsCliente;
wEditCadastro widgetsEdit;
wEditCliente  widgetsECliente;
wEditAdvog    widgetsEAdvog;
wNovoModelo   widgetsModelo;
wEditModelo   widgetsEModelo;
wConfirmar    widgetsConf;

/*Declara��o dos objetos globais*/
ADVOGADO advogLogado;
CLIENTE clienteEdit;
ADVOGADO advogBusca;
PROCESSO processoBusca;
MYSQL conexao;

/*Declara��o das fun��es*/

/*Fun��es relacionadas ao controle de widgets*/
G_MODULE_EXPORT void abreWidget ( GtkButton*, gpointer );
G_MODULE_EXPORT void destroy    ( GtkWidget*, gpointer );
G_MODULE_EXPORT void esconde    ( GtkWidget*, gpointer );

/*Fun��es relacionadas ao Login/Logout*/
G_MODULE_EXPORT void enviaLogin        ( GtkButton*, gpointer );
G_MODULE_EXPORT void enviaLogout       ( GtkButton*, gpointer );

/*Fun��es relacionadas ao cadastro de advogados*/
G_MODULE_EXPORT void enviaAddAdvog    ( GtkButton*, gpointer );
G_MODULE_EXPORT void enviaEditaAdvog  ( GtkButton*, gpointer );

/*Fun��es relacionadas ao cadastro de clientes*/
G_MODULE_EXPORT void enviaAddCliente   ( GtkButton*, gpointer );
G_MODULE_EXPORT void enviaEditaCliente ( GtkButton*, gpointer );

/*Fun��es relacionadas ao cadastro de processos*/
G_MODULE_EXPORT void enviaAddProcesso   ( GtkButton*, gpointer );
G_MODULE_EXPORT void enviaEditaProcesso ( GtkButton*, gpointer );

/*Fun��es relacionadas a busca de entidades*/
G_MODULE_EXPORT void enviaBuscaAdvog    ( GtkButton*, gpointer  );
G_MODULE_EXPORT void enviaBuscaCliente  ( GtkButton*, gpointer  );
G_MODULE_EXPORT void enviaBuscaProcesso ( GtkButton*, gpointer  );

/*Fun��es relacionadas a dele��o de entidades*/
G_MODULE_EXPORT void enviaDelCliente  ( GtkButton*, gpointer );

/*IMPLEMENTAR*/
static void enviaDelAdvog    ( GtkButton*, gpointer );
static void enviaDelProcesso ( GtkButton*, gpointer );

/*Misc*/
void mudaStatusBar    ( const gchar*, char* );
void getProcessoCorpo ( gchar* );




