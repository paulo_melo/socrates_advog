void enviaLogin (GtkButton *button, gpointer data)
{
        const gchar *entry1 = gtk_entry_get_text( GTK_ENTRY(widgetsLogin.entry1) );
        const gchar *entry2 = gtk_entry_get_text( GTK_ENTRY(widgetsLogin.entry2) );
        int tamanhoEntry1   = strlen( entry1 );
        int tamanhoEntry2   = strlen( entry2 );

        if (tamanhoEntry1 != 0 && tamanhoEntry2 != 0)
        {
            advogLogado.advogOAB = atoi(entry1);
            strcpy(advogLogado.advogPass, entry2);
            int retorno = loginAdvogado(&advogLogado, conexao);

            /*Verifica quanto a poss�veis erros. As constantes s�o auto explicativas.*/
            if (retorno == ADVOG_NOT_EXIST)
            {
                gtk_label_set_text ( widgetsLogin.labelError, AdvogNaoExiste);
                gtk_widget_show    ( widgetsLogin.imageError );
                gtk_widget_show    ( GTK_WIDGET ( widgetsLogin.labelError ) );
            }

            else if (retorno == ADVOG_PASS_DONT_MATCH)
            {
                gtk_label_set_text ( widgetsLogin.labelError, SenhaIncorreta);
                gtk_widget_show    ( widgetsLogin.imageError );
                gtk_widget_show    ( GTK_WIDGET ( widgetsLogin.labelError ) );
            }

            else if (retorno == ADVOG_LOG_FAILED)
            {
                gtk_label_set_text ( widgetsLogin.labelError, FalhaNoLogin );
                gtk_widget_show    ( widgetsLogin.imageError );
                gtk_widget_show    ( GTK_WIDGET ( widgetsLogin.labelError ) );
            }

            else
            {
                gtk_widget_hide ( widgetsLogin.window );
                const gchar msgStatus[40];
                sprintf ( (char*) msgStatus, "Bem vindo %s", advogLogado.advogNome );
                mudaStatusBar ( msgStatus, "Login" );
            }
        }
        else
        {
            gtk_widget_show ( widgetsLogin.imageError );
            gtk_widget_show ( GTK_WIDGET ( widgetsLogin.labelError ) );
        }
}

void enviaLogout (GtkButton *button, gpointer data)
{
    logoutAdvogado  ( advogLogado, conexao );
    const gchar msgStatus[40];
    sprintf( (char*) msgStatus, "%s deslogado com sucesso.", advogLogado.advogNome);
    advogLogado.advogId = 0;
    advogLogado.advogAdm = 0;
    advogLogado.advogOAB = 0;
    strcpy ( advogLogado.advogNome,  "" );
    strcpy ( advogLogado.advogPass,  "" );
    strcpy ( advogLogado.advogEmail, "" );
}

void enviaAddAdvog(GtkButton* button, gpointer data)
{
        const gchar *entry1 = gtk_entry_get_text ( GTK_ENTRY(widgetsAdvog.eOAB)   );
        const gchar *entry2 = gtk_entry_get_text ( GTK_ENTRY(widgetsAdvog.eName)  );
        const gchar *entry3 = gtk_entry_get_text ( GTK_ENTRY(widgetsAdvog.eCPF)   );
        const gchar *entry4 = gtk_entry_get_text ( GTK_ENTRY(widgetsAdvog.eEmail) );

        int tamanhoEntry1   = strlen( entry1 );
        int tamanhoEntry2   = strlen( entry2 );
        int tamanhoEntry3   = strlen( entry3 );
        int tamanhoEntry4   = strlen( entry4 );

        if (tamanhoEntry1 != 0 || tamanhoEntry2 != 0 || tamanhoEntry3 != 0 || tamanhoEntry4 != 0 )
        {
            ADVOGADO advogadoAdd;
            advogadoAdd.advogId = 0;
            advogadoAdd.advogOAB = atoi(entry1);
            strcpy(advogadoAdd.advogNome,  entry2);
            strcpy(advogadoAdd.advogPass,  entry3);
            strcpy(advogadoAdd.advogEmail, entry4);

            int retorno = inserirAdvogado(advogadoAdd, conexao);
            if ( retorno == ADVOG_REG_EXIST )
            {
                gtk_label_set_text ( widgetsAdvog.labelError, AdvogJaExiste);
                gtk_widget_show    ( widgetsAdvog.imageError );
                gtk_widget_show    ( GTK_WIDGET ( widgetsLogin.labelError ) );
            }
            else
            {

            }
        }
        else
        {
            gtk_widget_show( widgetsAdvog.imageError );
            gtk_widget_show( GTK_WIDGET ( widgetsLogin.labelError ) );
        }
}

void enviaBuscaAdvog (GtkButton* button, gpointer data)
{
/*gtk_entry_get_text : fun��o que retorna o texto que h� dentro das caixas de texto*/
const gchar *advogOAB = gtk_entry_get_text( GTK_ENTRY(widgetsMain.eBuscaAdvog)   );
    /* Convers�o para inteiro da string retornada */
    int oabBusca = atoi(advogOAB);
    int retorno = selecionaAdvogado(&advogBusca, oabBusca, conexao);
    char tempOAB[20];
    sprintf(tempOAB, "%d", advogBusca.advogOAB);

    gtk_entry_set_text (widgetsEAdvog.eOAB,   tempOAB  );
    gtk_entry_set_text (widgetsEAdvog.eName,  advogBusca.advogNome );
    gtk_entry_set_text (widgetsEAdvog.eSenha, advogBusca.advogPass );
    gtk_entry_set_text (widgetsEAdvog.eEmail, advogBusca.advogEmail);

    gtk_widget_show_all (widgetsEAdvog.window);
}

void enviaEditaAdvog (GtkButton* button, gpointer data)
{
/*gtk_entry_get_text : fun��o que retorna o texto que h� dentro das caixas de texto*/
    const gchar *advogOAB   = gtk_entry_get_text( GTK_ENTRY(widgetsEAdvog.eOAB)     );
    const gchar *advogNome  = gtk_entry_get_text( GTK_ENTRY(widgetsEAdvog.eName)    );
    const gchar *advogPass  = gtk_entry_get_text( GTK_ENTRY(widgetsEAdvog.eSenha)   );
    const gchar *advogEmail = gtk_entry_get_text( GTK_ENTRY(widgetsEAdvog.eEmail) );
    ADVOGADO advogadoUpd;

    advogadoUpd.advogId  = advogBusca.advogId;
    advogadoUpd.advogOAB = atoi(advogOAB);
    strcpy ( advogadoUpd.advogNome  , advogNome  );
    strcpy ( advogadoUpd.advogPass  , advogPass  );
    strcpy ( advogadoUpd.advogEmail , advogEmail );

    int retorno = atualizaAdvogado(advogadoUpd, conexao);
    if ( retorno == ADVOG_UPD_FAILED )
    {
            gtk_label_set_text ( widgetsModelo.labelError, "Erro ao atualizar");
            gtk_widget_show    ( widgetsModelo.imageError );
            gtk_widget_show    ( widgetsModelo.labelError );
    }
    else
    {
        gtk_widget_hide(widgetsEAdvog.window);
    }
}
