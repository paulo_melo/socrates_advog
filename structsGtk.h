typedef struct
{
    /* Janela */
    GtkWidget *window;
    GtkWidget *statusBar;
    /*Entries de busca*/
    GtkEntry *eBuscaAdvog, *eBuscaCliente, *eBuscaProcess;
    /*Bot�es da toolbar*/
    GtkButton *bCadastro, *bLogin, *bLogout, *bSair;
    /*Bot�es de busca*/
    GtkButton *bBuscaAdv, *bBuscaCli, *bProBusca;
    /*Bot�es de adicionar e Deletar clientes*/
    GtkButton *bCliAdd, *bCliDel;
    /*Bot�es de adicionar e Deletar processos*/
    GtkButton *bProAdd, *bProDel;
    /*Bot�es de adicionar e Deletar advogados*/
    GtkButton *bAdvAdd, *bAdvDel;

    GtkButton *bProPDF;
}wMain;

typedef struct
{
    /* Janela */
    GtkWidget *window;
    GtkEntry  *entry1, *entry2;
    GtkButton *button1, *button2;
    /* Widgets de erro */
    GtkLabel  *labelError;
    GtkWidget *imageError;
}wLogin;

typedef struct
{
    GtkWidget *window;
    GtkLabel  *labelError;
    GtkWidget *imageError;
    GtkEntry  *eOAB, *eName, *eCPF, *eRG, *eEnd, *eTel, *eEmail;
    GtkButton *bOk, *bCan;
}wAddAdvog;

typedef struct
{
    GtkWidget *window;
    GtkEntry  *eName, *eCpf, *eEnd, *eMail, *eTel;
    GtkLabel  *labelError;
    GtkWidget  *imageError;
    GtkButton *bOk, *bCan;

}wAddCliente;

typedef struct
{
    GtkWidget *window;
    GtkEntry  *eOAB, *eName, *eCPF, *eEnd, *eSenha, *eConf, *eTel, *eEmail;
    GtkLabel  *labelError;
    GtkWidget  *imageError;
    GtkButton *bOk, *bCan;
}wEditCadastro;

typedef struct
{
    GtkWidget *window, *tCorpo;
    GtkLabel  *labelError;
    GtkWidget *imageError;
    GtkEntry  *eTitulo, *eProto, *eRG;
    GtkButton *bCan, *bOk;
}wNovoModelo;

typedef struct
{
    GtkWidget *window, *tCorpo;
    GtkEntry  *eTitulo, *eRG, *eProto;
    GtkLabel  *labErroMod;
    GtkWidget *imErroMod;
    GtkButton *bOk, *bCan, *bDel, *bPdf;
}wEditModelo;


typedef struct
{
    GtkWidget *window;
    GtkLabel  *labelError;
    GtkWidget *imageError;
    GtkEntry  *eOAB, *eName, *eSenha, *eEmail;
    GtkButton *bOk, *bCan, *bDel;
}wEditAdvog;

typedef struct
{
    GtkWidget *window;
    GtkEntry  *eName, *eRG, *eEnd, *eMail, *eClientId;
    GtkLabel  *labelError;
    GtkWidget *imageError;
    GtkButton *bOk, *bCan, *bDel;
}wEditCliente;

typedef struct
{
    GtkWidget *window;
    GtkLabel  *lConfirm;
    GtkButton *bSim, *bNao;
}wConfirmar;
