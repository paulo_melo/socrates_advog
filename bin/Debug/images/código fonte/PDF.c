
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <setjmp.h>
#include "hpdf.h"

jmp_buf env;

#define INI_TITULO "<titulo>"
#define FIM_TITULO "</titulo>"
#define INI_TEXTO "<texto>"
#define FIM_TEXTO "</texto>"


#ifdef HPDF_DLL
void  __stdcall
#else
void
#endif
error_handler  (HPDF_STATUS   error_no,
                HPDF_STATUS   detail_no,
                void         *user_data)
{
    printf ("ERROR: error_no=%04X, detail_no=%u\n", (HPDF_UINT)error_no,
                (HPDF_UINT)detail_no);
    longjmp(env, 1);
}

//desenha a imagem no documento
void desenha_imagem (HPDF_Doc     pdf,
            const char  *filename,
            float        x,
            float        y,
            const char  *text,
            const char  *titulo)
{
#ifdef __WIN32__
    const char* FILE_SEPARATOR = "\\";
#else
    const char* FILE_SEPARATOR = "/";
#endif
    char filename1[255];

    HPDF_Page page = HPDF_GetCurrentPage (pdf);
    HPDF_Image image;

    strcpy(filename1, "images");
    strcat(filename1, FILE_SEPARATOR);
    strcat(filename1, filename);

    //carrega a imagem do local
    image = HPDF_LoadJpegImageFromFile (pdf, filename1);

    /* Desenha imagem na tela */
    HPDF_Page_DrawImage (page, image, x, y, HPDF_Image_GetWidth (image),
                HPDF_Image_GetHeight (image));
}

//fun��o para gerar o PDF, que recebe um ponteiro para a string completa.
int gerar_PDF (char *all, char *Nome_Arq)
{
    HPDF_Doc  pdf;
    HPDF_Page page;
    char fname[256];
    // esta recebe o conteudo de texto
    char texto[10000]={};
    //esta array recebe o cote�do do titulo
    char titulo [256];
    HPDF_Font font;
    HPDF_REAL altura_pagina;
    HPDF_REAL largura_pagina;
    HPDF_Rect rect;
    int i;
    HPDF_Destination dst;


    if(strstr(all, INI_TITULO) != 0)
    {
         filtra_titulo(all, titulo);
    }
    if(strstr(all, INI_TEXTO) != 0)
    {
         filtra_texto(all, texto);
    }

    strcpy (fname, Nome_Arq);
    strcat (fname, ".pdf");

    pdf = HPDF_New (error_handler, NULL);
    if (!pdf) {
        printf ("error: cannot create PdfDoc object\n");
        return 1;
    }

    /* error-handler */
    if (setjmp(env)) {
        HPDF_Free (pdf);
        return 1;
    }

    HPDF_SetCompressionMode (pdf, HPDF_COMP_ALL);

    /* add a new page object. */
    page = HPDF_AddPage (pdf);

    HPDF_Page_SetSize (page, HPDF_PAGE_SIZE_A5, HPDF_PAGE_PORTRAIT);

    altura_pagina = HPDF_Page_GetHeight (page);
    largura_pagina = HPDF_Page_GetWidth (page);

    font = HPDF_GetFont (pdf, "Times-Roman", NULL);
    HPDF_Page_SetTextLeading (page, 20);

    /*IMPLEMENTANDO IMAGEM*/
    dst = HPDF_Page_CreateDestination (page);
    HPDF_Destination_SetXYZ (dst, 0, HPDF_Page_GetHeight (page), 1);
    HPDF_SetOpenAction(pdf, dst);

    desenha_imagem (pdf, "imagem.jpg", 0, altura_pagina-90,
                "24bit color image");

    /* HPDF_TALIGN_CENTER*/
    rect.left = 50;
    rect.right = largura_pagina - 50;
    rect.top = altura_pagina - 100;
    rect.bottom = altura_pagina - 100;

    HPDF_Page_BeginText (page);

    HPDF_Page_SetFontAndSize (page, font, 12);
    HPDF_Page_TextRect (page, rect.left, rect.top, rect.right-5, rect.bottom, titulo, HPDF_TALIGN_CENTER, NULL);

    HPDF_Page_EndText (page);

    /* HPDF_TALIGN_JUSTIFY */
    rect.top = altura_pagina - 150;
    rect.bottom = 50;

    HPDF_Page_BeginText (page);

    HPDF_Page_SetFontAndSize (page, font, 12);
    HPDF_Page_TextRect (page, rect.left, rect.top, rect.right-5, rect.bottom, texto, HPDF_TALIGN_JUSTIFY, NULL);

    HPDF_Page_EndText (page);

    /* save the document to a file */
    HPDF_SaveToFile (pdf, fname);

    /* clean up */
    HPDF_Free (pdf);

    return 0;
}

