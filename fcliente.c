void enviaDelCliente ( GtkButton* button, gpointer data)
{

}

void enviaAddCliente (GtkButton* button, gpointer data)
{
        /*gtk_entry_get_text : fun��o que busca o texto que h� dentro das caixas de texto*/
        const gchar *clienteNome  = gtk_entry_get_text( GTK_ENTRY( widgetsCliente.eName  ) );
        const gchar *clienteRG    = gtk_entry_get_text( GTK_ENTRY( widgetsCliente.eCpf   ) );
        const gchar *clienteEnd   = gtk_entry_get_text( GTK_ENTRY( widgetsCliente.eEnd   ) );
        const gchar *clienteEmail = gtk_entry_get_text( GTK_ENTRY( widgetsCliente.eMail  ) );

        int tamanhoEntry1   =  strlen( clienteNome  );
        int tamanhoEntry2   =  strlen( clienteRG    );
        int tamanhoEntry3   =  strlen( clienteEnd   );
        int tamanhoEntry4   =  strlen( clienteEmail );

        if (tamanhoEntry1 != 0 || tamanhoEntry2 != 0 || tamanhoEntry3 != 0 || tamanhoEntry4 != 0 )
        {
            /*Se todos os campos n�o estiverem em branco, a inser��o eh realizada*/
            /*Ap�s buscar o texto, constr�i-se a estrutura que representa o cliente*/
            CLIENTE clienteAdd;
            clienteAdd.advogId = advogLogado.advogId;
            strcpy ( clienteAdd.clienteNome  , clienteNome  );
            strcpy ( clienteAdd.clienteEnd   , clienteEnd   );
            strcpy ( clienteAdd.clienteRG    , clienteRG    );
            strcpy ( clienteAdd.clienteEmail , clienteEmail );
            /*Depois que a estrutura � constr�ida, envia-se como argumento da fun��o e o cliente ser� cadastrado*/
            int retorno = inserirCliente(clienteAdd, conexao);
            if ( retorno == CLIENT_RG_EXIST )
            {
                gtk_label_set_text ( widgetsCliente.labelError, CpfClienteExiste);
                gtk_widget_show    ( widgetsCliente.imageError );
                gtk_widget_show    ( GTK_WIDGET ( widgetsLogin.labelError ) );
            }
            else if ( retorno == CLIENT_REG_FAILED )
            {
                gtk_label_set_text ( widgetsCliente.labelError, FalhaNoRegistro);
                gtk_widget_show    ( widgetsCliente.imageError );
                gtk_widget_show    ( GTK_WIDGET ( widgetsLogin.labelError ) );
            }
            else
            {
                const gchar msgStatus[40];
                gtk_widget_hide( widgetsCliente.window );
            }
        }
        else
        {
            gtk_widget_show( widgetsCliente.imageError );
            gtk_widget_show( GTK_WIDGET ( widgetsLogin.labelError ) );
        }
}

void enviaBuscaCliente (GtkButton* button, gpointer data)
{
/*gtk_entry_get_text : fun��o que retorna o texto que h� dentro das caixas de texto*/
const gchar *entry1 = gtk_entry_get_text( GTK_ENTRY(widgetsMain.eBuscaCliente)   );

    strcpy(clienteEdit.clienteRG, entry1);
    CLIENTE *clienteBuff = &clienteEdit;

    int retorno = selecionaCliente(clienteBuff, clienteEdit.clienteRG, advogLogado.advogId, conexao);

    gtk_entry_set_text ( widgetsECliente.eName, clienteEdit.clienteNome  );
    gtk_entry_set_text ( widgetsECliente.eRG  , clienteEdit.clienteRG    );
    gtk_entry_set_text ( widgetsECliente.eEnd , clienteEdit.clienteEnd   );
    gtk_entry_set_text ( widgetsECliente.eMail, clienteEdit.clienteEmail );

    gtk_widget_show_all ( widgetsECliente.window );
}

void enviaEditaCliente (GtkButton* button, gpointer data)
{
/*gtk_entry_get_text : fun��o que retorna o texto que h� dentro das caixas de texto*/
    const gchar *clienteNome  = gtk_entry_get_text( GTK_ENTRY(widgetsECliente.eName)    );
    const gchar *clienteRG    = gtk_entry_get_text( GTK_ENTRY(widgetsECliente.eRG)      );
    const gchar *clienteEnd   = gtk_entry_get_text( GTK_ENTRY(widgetsECliente.eEnd)     );
    const gchar *clienteEmail = gtk_entry_get_text( GTK_ENTRY(widgetsECliente.eMail)    );


    CLIENTE clienteUpd;
    clienteUpd.advogId   = advogLogado.advogId;
    clienteUpd.clienteId = clienteEdit.clienteId;
    strcpy ( clienteUpd.clienteNome  , clienteNome    );
    strcpy ( clienteUpd.clienteRG    , clienteRG      );
    strcpy ( clienteUpd.clienteEnd   , clienteEnd     );
    strcpy ( clienteUpd.clienteEmail , clienteEmail   );

   int retorno = atualizaCliente(clienteUpd, conexao);
    if ( retorno == CLIENT_UPD_FAILED )
    {

    }
    else
    {
        gtk_widget_hide(widgetsECliente.window);
    }
}
