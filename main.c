#include <windows.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <hpdf.h>
#include <setjmp.h>
#include "funcoes.h"
#include "funcoesGtk.h"

jmp_buf env;
int error_handler  (HPDF_STATUS   error_no,
                HPDF_STATUS   detail_no,
                void         *user_data)
{
    printf ("ERROR: error_no=%04X, detail_no=%u\n", (HPDF_UINT)error_no,
                (HPDF_UINT)detail_no);
    longjmp(env, 1);
}
void desenha_imagem (HPDF_Doc     pdf,
            const char  *filename,
            float        x,
            float        y,
            const char  *text
                )
{
#ifdef __WIN32__
    const char* FILE_SEPARATOR = "\\";
#else
    const char* FILE_SEPARATOR = "/";
#endif
    char filename1[255];

    HPDF_Page page = HPDF_GetCurrentPage (pdf);
    HPDF_Image image;

    strcpy(filename1, "images");
    strcat(filename1, FILE_SEPARATOR);
    strcat(filename1, filename);

    image = HPDF_LoadJpegImageFromFile (pdf, filename1);

    /* Desenha imagem na tela */
    HPDF_Page_DrawImage (page, image, x, y, HPDF_Image_GetWidth (image),
                HPDF_Image_GetHeight (image));
}

int gerar_PDF (char *titulo, char *texto)
{
    HPDF_Doc  pdf;
    HPDF_Page page;
    char fname[256];

    HPDF_Font font;
    HPDF_REAL altura_pagina;
    HPDF_REAL largura_pagina;
    HPDF_Rect rect;
    int i;
    HPDF_Destination dst;

    strcpy (fname, processoBusca.processoTitulo);
    strcat (fname, ".pdf");

    pdf = HPDF_New (error_handler, NULL);
    if (!pdf) {
        printf ("error: cannot create PdfDoc object\n");
        return 1;
    }

    /* error-handler */
    if (setjmp(env)) {
        HPDF_Free (pdf);
        return 1;
    }

    HPDF_SetCompressionMode (pdf, HPDF_COMP_ALL);

    /* add a new page object. */
    page = HPDF_AddPage (pdf);

    HPDF_Page_SetSize (page, HPDF_PAGE_SIZE_A5, HPDF_PAGE_PORTRAIT);

    altura_pagina = HPDF_Page_GetHeight (page);
    largura_pagina = HPDF_Page_GetWidth (page);

    font = HPDF_GetFont (pdf, "Times-Roman", NULL);
    HPDF_Page_SetTextLeading (page, 20);

    /*IMPLEMENTANDO IMAGEM*/
    dst = HPDF_Page_CreateDestination (page);
    HPDF_Destination_SetXYZ (dst, 0, HPDF_Page_GetHeight (page), 1);
    HPDF_SetOpenAction(pdf, dst);

    desenha_imagem (pdf, "imagem.jpg", 0, altura_pagina-90,
                "24bit color image");

    /* HPDF_TALIGN_CENTER*/
    rect.left = 50;
    rect.right = largura_pagina - 50;
    rect.top = altura_pagina - 100;
    rect.bottom = altura_pagina - 100;

    HPDF_Page_BeginText (page);

    HPDF_Page_SetFontAndSize (page, font, 12);
    HPDF_Page_TextRect (page, rect.left, rect.top, rect.right-5, rect.bottom, titulo, HPDF_TALIGN_CENTER, NULL);

    HPDF_Page_EndText (page);

    /* HPDF_TALIGN_JUSTIFY */
    rect.top = altura_pagina - 150;
    rect.bottom = 50;

    HPDF_Page_BeginText (page);

    HPDF_Page_SetFontAndSize (page, font, 12);
    HPDF_Page_TextRect (page, rect.left, rect.top, rect.right-5, rect.bottom, texto, HPDF_TALIGN_JUSTIFY, NULL);

    HPDF_Page_EndText (page);

    /* save the document to a file */
    HPDF_SaveToFile (pdf, fname);

    /* clean up */
    HPDF_Free (pdf);

    return 0;
}


int main( int argc, char **argv )
{

mysql_init(&conexao);
if (mysql_real_connect(&conexao, "127.0.0.1", "root", "123456", "advogcontrol", 0, NULL, 0))
{
            GtkBuilder *builder, *editModelo;
            gtk_init( &argc, &argv );

        /*Inicializando o arquivo .glade*/
            builder = gtk_builder_new();
                gtk_builder_add_from_file( builder, "Project.glade", NULL );

            /*POG: Janela de modelos n�o mostrava nenhum widget se carregado a partir do mesmo arquivo glade*/
            editModelo = gtk_builder_new();
                gtk_builder_add_from_file( editModelo, "editModelo.glade", NULL );

        /*Buscando objetos principais da janela*/
            buildMain(builder, &widgetsMain);

        /*Construindo janelas*/
            buildLogin       ( builder, &widgetsLogin      );
            buildAdvog       ( builder, &widgetsAdvog      );
            buildCliente     ( builder, &widgetsCliente    );
            buildModelo      ( builder, &widgetsModelo     );
            buildEditCliente ( builder, &widgetsECliente   );
            buildEditAdvog   ( builder, &widgetsEAdvog     );
            buildConf        ( builder, &widgetsConf       );
            buildEditModelo  ( editModelo, &widgetsEModelo );

        /*Liberando os builders*/
            g_object_unref (G_OBJECT (builder));
            g_object_unref (G_OBJECT (editModelo));

        /*Manualmente conectando os sinais dos objetos*/

            /*Conectando sinais dos bot�es da janela main*/

            g_signal_connect ( G_OBJECT (widgetsMain.window),       "destroy", G_CALLBACK(destroy), NULL );
            g_signal_connect ( G_OBJECT (widgetsMain.bSair  ),      "clicked", G_CALLBACK(destroy), NULL );
            g_signal_connect ( G_OBJECT (widgetsMain.bLogin ),      "clicked", G_CALLBACK(abreWidget), widgetsLogin.window   );
            g_signal_connect ( G_OBJECT (widgetsMain.bAdvAdd),      "clicked", G_CALLBACK(abreWidget), widgetsAdvog.window   );
            g_signal_connect ( G_OBJECT (widgetsMain.bCliAdd),      "clicked", G_CALLBACK(abreWidget), widgetsCliente.window );
            g_signal_connect ( G_OBJECT (widgetsMain.bProAdd),      "clicked", G_CALLBACK(abreWidget), widgetsModelo.window  );
            g_signal_connect ( G_OBJECT (widgetsMain.bLogout),      "clicked", G_CALLBACK(enviaLogout), NULL                 );
            g_signal_connect ( G_OBJECT (widgetsMain.bBuscaAdv),    "clicked", G_CALLBACK(enviaBuscaAdvog), NULL             );
            g_signal_connect ( G_OBJECT (widgetsMain.bBuscaCli),    "clicked", G_CALLBACK(enviaBuscaCliente), NULL           );
            g_signal_connect ( G_OBJECT (widgetsMain.bProBusca),    "clicked", G_CALLBACK(enviaBuscaProcesso), NULL          );

            /*Bot�es de escolha da janela de Login*/
            g_signal_connect ( G_OBJECT (widgetsLogin.button2),     "clicked", G_CALLBACK(esconde), widgetsLogin.window );
            g_signal_connect ( G_OBJECT (widgetsLogin.button1),     "clicked", G_CALLBACK(enviaLogin), NULL             );

            /*Bot�es de escolha da janela de adicionar advogado*/
            g_signal_connect ( G_OBJECT (widgetsAdvog.bOk),         "clicked", G_CALLBACK(enviaAddAdvog), NULL          );
            g_signal_connect ( G_OBJECT (widgetsAdvog.bCan),        "clicked", G_CALLBACK(esconde), widgetsAdvog.window );

            /*Bot�es de escolha da janela de adicionar cliente*/
            g_signal_connect ( G_OBJECT (widgetsCliente.bOk),       "clicked", G_CALLBACK(enviaAddCliente), NULL          );
            g_signal_connect ( G_OBJECT (widgetsCliente.bCan),      "clicked", G_CALLBACK(esconde), widgetsCliente.window );

            /*Bot�es de escolha da janela de adicionar um processo*/
            g_signal_connect ( G_OBJECT (widgetsModelo.bOk ),       "clicked", G_CALLBACK(enviaAddProcesso), NULL        );
            g_signal_connect ( G_OBJECT (widgetsModelo.bCan),       "clicked", G_CALLBACK(esconde), widgetsModelo.window );

            /*Bot�es de escolha da janela de editar os dados de um cliente*/
            g_signal_connect ( G_OBJECT (widgetsECliente.bOk),      "clicked", G_CALLBACK(enviaEditaCliente), NULL         );
            g_signal_connect ( G_OBJECT (widgetsECliente.bDel),     "clicked", G_CALLBACK(enviaDelCliente), NULL           );
            g_signal_connect ( G_OBJECT (widgetsECliente.bCan),     "clicked", G_CALLBACK(esconde), widgetsECliente.window );

            /*Bot�es de escolha da janela de editar os dados de um advogado*/
            g_signal_connect ( G_OBJECT (widgetsEAdvog.bOk),        "clicked", G_CALLBACK(enviaEditaAdvog), NULL );

            /*Bot�o de confirma��o do di�logo de confirma��o*/
            g_signal_connect ( G_OBJECT (widgetsConf.bSim),     "clicked", G_CALLBACK(esconde), widgetsConf.window );

            gtk_widget_show_all(widgetsMain.window);

            gtk_main();
}
return( 0 );
}


