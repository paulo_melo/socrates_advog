#include <gtk/gtk.h>
#include "funcoes.h"
#include "funcoesGtk.h"


#include "fadvogado.c"
#include "fcliente.c"
#include "fprocesso.c"

void abreWidget (GtkButton *button, gpointer data)
{
    gtk_widget_show_all(data);
}

void destroy (GtkWidget *window, gpointer data)
{
    gtk_main_quit ();
}

void esconde (GtkWidget *window, gpointer data)
{
    gtk_widget_hide (data);
}

void mudaStatusBar(const gchar* msgStatus, char* contextId)
{
    guint idMsg = gtk_statusbar_get_context_id(widgetsMain.statusBar, contextId);
    gtk_statusbar_push(widgetsMain.statusBar, idMsg, msgStatus);
    gtk_widget_hide (widgetsLogin.window);
}

void getProcessoCorpo(gchar* processoCorpo)
{
    GtkTextIter iterInicio;
    GtkTextIter iterFinal;
    GtkTextBuffer* buffer = gtk_text_view_get_buffer( widgetsModelo.tCorpo );
        gtk_text_buffer_get_start_iter ( buffer, &iterInicio );
        gtk_text_buffer_get_end_iter   ( buffer, &iterFinal  );
 processoCorpo     = gtk_text_buffer_get_text( buffer, &iterInicio, &iterFinal, FALSE);
}


