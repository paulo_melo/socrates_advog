void enviaAddProcesso (GtkButton* button, gpointer data)
{
gchar* processoCorpo     = "";
    getProcessoCorpo (processoCorpo);
gchar* clienteRG         = gtk_entry_get_text ( GTK_ENTRY ( widgetsModelo.eRG )      );
gchar* processoTitulo    = gtk_entry_get_text ( GTK_ENTRY ( widgetsModelo.eTitulo )  );
gchar* tempProcessoProto = gtk_entry_get_text ( GTK_ENTRY ( widgetsModelo.eProto )   );

int tamanhoCorpo  = strlen(processoCorpo);
int tamanhoRG     = strlen(clienteRG);
int tamanhoTitulo = strlen(processoTitulo);
int tamanhoProto  = strlen(tempProcessoProto);

if (tamanhoCorpo != 0 || tamanhoProto != 0 || tamanhoRG != 0 || tamanhoTitulo != 0 )
{

    CLIENTE cliente;
    CLIENTE *clienteBufa = &cliente;

    int retorno = selecionaCliente(clienteBufa, clienteRG, advogLogado.advogId, conexao);

    if ( retorno == CLIENT_NOT_EXIST )
    {
    }
    else
    {
        PROCESSO processo;
        processo.advogId   = advogLogado.advogId;
        processo.clienteId = cliente.clienteId;
        processo.processoProto = atoi(tempProcessoProto);
        strcpy ( processo.processoTitulo, processoTitulo);
        strcpy ( processo.processoCorpo,  processoCorpo);
        retorno = inserirProcesso(processo, conexao);

        if ( retorno == PROCESS_REG_FAILED )
        {
            gtk_label_set_text ( widgetsModelo.labelError, FalhaNoRegistro);
            gtk_widget_show    ( widgetsModelo.imageError );
            gtk_widget_show    ( GTK_WIDGET ( widgetsLogin.labelError ) );
        }

        else
        {
            const gchar msgStatus[40];
            sprintf( msgStatus, "Processo de protocolo %d adicionado com sucesso", processo.processoProto );
            mudaStatusBar ( msgStatus, "processoAdd");
            gtk_widget_hide    ( widgetsModelo.imageError );
            gtk_widget_hide    ( GTK_WIDGET ( widgetsLogin.labelError ) );
            gtk_widget_hide    ( widgetsModelo.window );
        }
    }
}
else
{
    gtk_widget_show    ( widgetsModelo.imageError );
    gtk_widget_show    ( GTK_WIDGET ( widgetsLogin.labelError ) );
}
}

void enviaBuscaProcesso (GtkButton* button, gpointer data)
{
    const gchar *entry1 = gtk_entry_get_text( GTK_ENTRY(widgetsMain.eBuscaProcess)  );

    processoBusca.advogId = advogLogado.advogId;
    PROCESSO *processoBuff = &processoBusca;
    int processoProto = atoi(entry1);
    int retorno = selecionaProcesso (processoBuff, processoProto, conexao);
    if (retorno == PROCESS_NOT_EXIST)
    {

    }
    else
    {
        char temp[20];
        sprintf(temp, "%d", processoBusca.processoProto);
        gtk_entry_set_text ( widgetsEModelo.eTitulo, processoBusca.processoTitulo  );
        g_printf("%s", processoBusca.processoTitulo);
        gtk_entry_set_text ( widgetsEModelo.eProto,  temp);

        GtkTextBuffer* buffer = gtk_text_view_get_buffer( widgetsEModelo.tCorpo );
        gtk_text_buffer_set_text(buffer, processoBusca.processoCorpo, -1 );

  /*  gerar_PDF (processoBusca.processoTitulo, processoBusca.processoCorpo); */
    gtk_widget_show_all(widgetsEModelo.window);
    }
}

void enviaEditaProcesso (GtkButton* button, gpointer data)
{

}
